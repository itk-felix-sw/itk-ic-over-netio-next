
# Introduction
itk-ic-over-netio-next tool is a tool is a mini-software library that was designed for FELIX hardware, that allows IC commands to be sent over felixcore (netio) for LPGBT I2C coumminication.
This software is based on the NSW project's ic-over-netio tool but is heavily modified. 
The software is written in C++ and can be used as a c++ class or a python module. 


# Compile instructions for FELIX envoriment

Compilation is currently only possible within felixcore.
Steps:
1) Clone this library in felix software directory
2) Edit the <felix-sw-dir>/cmake_tdaq/bin/setup.sh by adding the following line next to other PYTHONPATH lists 
```
export PYTHONPATH=${BINARY_TAG_PATH}/itk-ic-over-netio-next:${PYTHONPATH}
```

Then follow normal felix software compiling instructions.

---
# Compile Instructions for TDAQ envoriment
The package dependencies are kept at a minimum only PYBIND11 and TBB packages are required
For machines with CVMFS acceses you should be able to compile this package out of the box following these instructions

```
git clone ssh://git@gitlab.cern.ch:7999/itk-felix-sw/itk-felix-sw.git
cd itk-felix-sw
git clone ssh://git@gitlab.cern.ch:7999/itk-felix-sw/itk-ic-over-netio-next.git
source setup.sh
mkdir build
cd build
cmake ../
make -j install
```

If you are using the TDAQ envoriment setup, you would need to source felix libraries. You can do this two ways:
1) After you compile and software, you need to install pre-compiled felix-libraries. you need to do this only once
```
cd itk-felix-sw/
python share/copy_netionext_to_external.py -f <Directory of raw felix files location, as example: /cvmfs/atlas-online-nightlies.cern.ch/felix/releases/felix-05-00-01-rm5-stand-alone/x86_64-centos7-gcc11-opt/lib/>
``` 
2) Alternativly you sourcce the following lines every time you setup itk-felix-sw
```
#export LD_LIBRARY_PATH=<PATH FOR PRECOMPILED FELIX LIBRARIES>:$LD_LIBRARY_PATH; 
# Exmaple of it is below
export LD_LIBRARY_PATH=/cvmfs/atlas-online-nightlies.cern.ch/felix/releases/felix-05-00-01-rm5-stand-alone/x86_64-centos7-gcc11-opt/lib/:$LD_LIBRARY_PATH
```
For the second option, you need to repeat the prodecedure every time you source the envoriment



---
# How to use in itk-felix-sw envoriment


Then you can checkout the use example on the ic-over-netio/test.py example. 
Main idea is you connect to a given Felix server in a given channel using (Python): 
```
from libitk_ic_over_netio_next import *
IC = ICNetioNextHandler("lo',"/tmp/bus", 0x1000000000118000, 0x10000000001d0000, False, 0, 0x74, "lo")
IC.readCfg()
```
where the individual fields are:
```
ICNetioNextHandler(<IP of FELIX Machine>,
	  <TX PORT>, 
	  <RX PORT>, 
	  <Elink id for IC CMD  generaly 17+64xFiberID>,
	  <Elink id for IC Data generaly 29+64xFiberID>	,
	  <Attempt to re-subscribe if communication fails>,
	  <LpGBT Version, currently 0,1 is supported>,
	  <I2C identified of the MASTER hadrware>,
	  <Communication Interafece>,
	  <The bus directory created by felix>
```

Then individual IC commands can be sent to

1. read all LpGBT registers at once
```
IC.readCfg()
```

2. write all LpGBT registers at once
```
IC.sendCfg(<Array of all register values>)
```

3. read registers
```
IC.readRegs([<"ARRAY of REGISTER NAMES">])
```
4. write registers
```
IC.sendRegs({ <REGISTERNAME>:[<Values to write>] })
```