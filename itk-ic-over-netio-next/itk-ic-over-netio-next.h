#ifndef __IC_NETIONEXT_HANDLER__
#define __IC_NETIONEXT_HANDLER__

#include <string>
#include <vector>
#include <cstdint>


#ifndef FELIX_BUILD
#include <felix_proxy/ClientThread.h>
#endif 

#ifdef FELIX_BUILD
#include "felix/felix_client_thread_impl.hpp"
#endif 


class ICNetioNextHandler
{
  public:
    enum class DeviceType {
      	lpGBT_v0,
	lpGBT_v1,
    };

    ICNetioNextHandler() = delete;

    ICNetioNextHandler(
	      std::string interface,	      
	      std::string buspath,
              uint64_t fID_CMD,
	      uint64_t fID_R, 
	      bool resubscribeValue = false, 
	      unsigned int LpGBTVersion=0, 
	      uint8_t I2CPort=0x74);
    ~ICNetioNextHandler();

    uint32_t CommTests(unsigned NTries, std::string Register);

    void setMaxRetries(int);

    void sendCfg(std::string);

    // ATTENTION! those below will be useful for interfacing
    // will be useful for outside interfacing
    void sendCfg(const std::vector<uint8_t> &);

    // send to register(key)
    //   all the values in vector(item)
    //   e.g. { 0 => { '0xff', '0x00' }, 1 => { '0x02' } } means send to register 0 0xff, then 0x00; send to register 1, 0x02
    //   the order of register follows (auto & reg: map)
    void sendRegs(std::map<std::string, std::vector<uint8_t>>);

    // bool isConnected();

    std::vector<uint8_t> readCfg();
    // takes a vector of address 
    // returns a vector of pair<addr, val>
    std::vector<std::pair<std::string, uint8_t>> readRegs(std::vector<std::string>);

  private:
    
    // prepare the netio frame with proper register address and data(if write)
    std::vector<uint8_t> prepareICNetioFrame(bool read, uint16_t startAddr, const std::vector<uint8_t>& data);

    // newly added function, called by interfaces
    std::vector<unsigned char> prepareNetioFrame(bool read, uint16_t startAddr, const std::vector<uint8_t>& data);

    /**
     * Carry out the actual netio communication.
     * Expect a reply for every netio fragment.
     * Check the rx every waitmillis, for a total of maxattempts.
     * If resubscribeonretry is set, the netio communication will be closed and opened again before every new attempt.
     * @param netioframe the byte contents of the message to send
     * @param waitmillis the number of milliseconds to wait before the next attempt to communicate
     * @return true if commnunication was possible 
     */
    bool communicate(std::vector<uint8_t> & netioframe, uint32_t waitmillis=5);

    
#ifndef FELIX_BUILD
    felix_proxy::ClientThread * m_NextClient;
    felix_proxy::ClientThread::Config  *m_NextConfig;
#endif
#ifdef FELIX_BUILD
    FelixClientThreadImpl * m_NextClient;
    FelixClientThreadInterface::Config *m_NextConfig;
#endif


    std::vector<uint8_t> m_reply;
    
    unsigned int m_maxRetries;
    bool m_resubscribe;
    bool m_clear;
    uint64_t m_fID_CMD;
    uint64_t m_fID_R;
    DeviceType m_icDevice;   ///< Type of device that this ICNetioNextHandler works with
    std::uint8_t m_i2cAddr;      ///< I2C address this ICNetioNextHandler will prepare frames for
    std::vector<uint64_t> m_vec_fids;
    bool m_verbose;
    std::string m_interface;
    std::string m_buspath;

  protected:

  public:
    constexpr static std::size_t NUM_GBTX_READABLE_REGISTERS{436}; ///< The GBTx has 436 total registers

    constexpr static std::size_t NUM_BYTES_IC_HEADER_V0{7};          ///< First 7 bytes of IC reply are header
    constexpr static std::size_t NUM_BYTES_IC_HEADER_V1{6};          ///< First 7 bytes of IC reply are header
    constexpr static std::size_t NUM_PARITY_BYTES_IC_TRAILER{1};  ///< Last byte of IC reply is parity word
    constexpr static std::size_t FIRST_IC_PAYLOAD_BYTE_V0{NUM_BYTES_IC_HEADER_V0};
    constexpr static std::size_t FIRST_IC_PAYLOAD_BYTE_V1{NUM_BYTES_IC_HEADER_V1};
};


#endif
