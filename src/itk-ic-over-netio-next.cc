#include "itk-ic-over-netio-next/itk-ic-over-netio-next.h"
#include <chrono>
#include <exception>
#include <fstream>

#include "itk-ic-over-netio-next/lpgbt-items-v0.h"
#include "itk-ic-over-netio-next/lpgbt-items-v1.h"

#include "pybind11/pybind11.h"
#include "pybind11/stl.h"

struct FelixCmdHeader{
  uint32_t length;
  uint32_t reserved;
  uint64_t elink;
};

using namespace std;

const lpgbt_item_t * ParseRegister(const char * name, ICNetioNextHandler::DeviceType device) {

  
  int  reg_addr   = -1;
  const char *fieldname  = 0;

  // Check if a (decimal or hex) number is provided
  // which is to be taken as a register address,
  // else consider it to be a register or bit field name
  bool is_hex = true;
  bool is_dec = true;
  for( unsigned int i=0; i<strlen(name); ++i )
    {
      char ch = name[i];
      if( (i == 0 && ch != '0') ||
	  (i == 1 && ch != 'x' && ch != 'X') ||
	  (i >= 2 &&
	   !((ch >= 'a' && ch <= 'f') ||
	     (ch >= 'A' && ch <= 'F') ||
	     (ch >= '0' && ch <= '9'))) )
	is_hex = false;
 
      if( ch < '0' || ch > '9' )
	is_dec = false;
    }

  if( is_dec )
    {
      // Not a register address after all?
      if( sscanf( name, "%d", &reg_addr ) != 1 )
	fieldname = name;
    }
  else if( is_hex )
    {
      // Not a register address after all?
      if( sscanf( name, "%x", (uint32_t*)&reg_addr ) != 1 )
	fieldname = name;
    }
  else
    {
      fieldname = name;
    }


  const lpgbt_item_t *item_list=0;
  if( device==ICNetioNextHandler::DeviceType::lpGBT_v0 )
    item_list = LPGBTv0_ITEM;
  else if( device==ICNetioNextHandler::DeviceType::lpGBT_v1 )
    item_list = LPGBTv1_ITEM; 

  const lpgbt_item_t *item = &item_list[0];
  while( strlen(item->name) != 0 )
    {
      
      if( fieldname!=0 && strcmp(item->name, fieldname) == 0 )
        {
          break;
        }
      else if ( reg_addr!=-1 && item->addr == reg_addr) {
          break;
      }
      
      ++item;
    }


  return item;



}



uint8_t strTouint8(std::string str)
{
  if(str.size() > 2 || str.size() == 0)
    throw std::logic_error("\"" + str + "\" is not a valid uint_8");

  uint8_t retVal = 0;

  for(size_t i = 0; i < str.size(); ++i)
    {
      retVal = retVal << 4;
      switch(str[i])
	{
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
	  retVal += static_cast<uint8_t>(str[i] - '0');
	  break;
	case 'a':
	case 'b':
	case 'c':
	case 'd':
	case 'e':
	case 'f':
	  retVal += static_cast<uint8_t>(str[i] - 'a') + 10;
	  break;
	default:
	  throw std::out_of_range("The value " + str + " is  not valid hexadecimal");
	}
    }

  return retVal;
}



namespace py = pybind11;
PYBIND11_MODULE(libitk_ic_over_netio_next, m) {
  py::class_<ICNetioNextHandler> (m, "ICNetioNextHandler")
    .def(py::init<std::string, std::string, const uint64_t, const uint64_t, const bool, const unsigned int , const uint8_t>())
    .def("setMaxRetries", (&ICNetioNextHandler::setMaxRetries),"Set Max Retries for writting")
    .def("sendCfg", static_cast<void (ICNetioNextHandler::*)(const std::vector<uint8_t> &)>(&ICNetioNextHandler::sendCfg),"Send the full register set")
    .def("sendRegs", (&ICNetioNextHandler::sendRegs),"Send the following registers")
    .def("readCfg", (&ICNetioNextHandler::readCfg),"Read the full set of registers", py::return_value_policy::copy)
    .def("readRegs", (&ICNetioNextHandler::readRegs),"Read the given set of registers", py::return_value_policy::copy)
    .def("CommTests", (&ICNetioNextHandler::CommTests),"Communication Test", py::return_value_policy::copy)

;
}






ICNetioNextHandler::ICNetioNextHandler(
		     const std::string interface,
		     const std::string buspath,
                     const uint64_t fID_CMD,
		     const uint64_t fID_R,
                     const bool resubscribeValue,
		     const unsigned int LpGBTVersion,
		     const uint8_t I2CPort
                     ){
  m_maxRetries = 10;
  m_resubscribe=resubscribeValue;
  m_fID_CMD=fID_CMD;
  m_fID_R=fID_R;
  m_icDevice=(LpGBTVersion==1?DeviceType::lpGBT_v1:DeviceType::lpGBT_v0);
  m_i2cAddr=I2CPort;
  m_interface=interface;
  m_buspath=buspath;
  m_verbose=false;
  m_clear=true;
  std::cout<<"Handler::Connect Interface to be used is:"<<m_interface<<endl;
  std::cout<<"Handler::Connect bus dir   to be used is:"<<m_buspath<<endl; 

  m_NextConfig = new FelixClientThreadInterface::Config();
  m_NextConfig->property["local_ip_or_interface"] = m_interface;//"127.0.0.1";
  
  if(m_verbose) // Error level is defined by verbosity level
    m_NextConfig->property["log_level"] = "trace";
  else
     m_NextConfig->property["log_level"] = "error";

  m_NextConfig->property["bus_dir"] = m_buspath;
  m_NextConfig->property["bus_group_name"] = "FELIX";
  // m_NextConfig.property["verbose_bus"] = "True";
  // m_NextConfig.property["netio_pages"] = "256";
  // m_NextConfig.property["timeout"] = "2";
  // m_NextConfig.property["netio_pagesize"] = "";
  // m_NextConfig.property["thread_affinity"] = "";

  

  m_NextConfig->on_init_callback = []() {
    std::cout<<"Handler::ClientThread::Initialiasing connection"<<endl;
  };

  m_NextConfig->on_connect_callback = [](const std::uint64_t fid){
    std::cout<<"Handler::ClientThread::Connecting to FID: 0x"<<std::hex<<fid<<std::dec<<endl;

  };

  m_NextConfig->on_disconnect_callback = [](const std::uint64_t fid){
    std::cout<<"Handler::ClientThread::Disconnecting to FID: 0x"<<std::hex<<fid<<std::dec<<endl;

  };




  //Shouldn't handle service frames yet
  m_NextConfig->on_data_callback =  [&](const std::uint64_t rx_elink,
						   const std::uint8_t* data,
						   const std::size_t size,
						   const std::uint8_t status){
    if(m_clear)
      m_reply.clear();
   
    m_reply.insert(m_reply.begin(),data,data+size); //This is copy, not ideal
    
  };
  m_vec_fids = {m_fID_CMD,m_fID_R};
  #ifndef FELIX_BUILD
  m_NextClient = new felix_proxy::ClientThread(*m_NextConfig);
  #endif
  #ifdef FELIX_BUILD
  m_NextClient = new FelixClientThreadImpl(*m_NextConfig);
  #endif

  m_NextClient->subscribe(m_vec_fids);




  // m_tx=new netio::low_latency_send_socket(m_context);
  // m_tx->connect(netio::endpoint(m_flxHost,m_portToGBTx));
  // if(!m_tx->is_open()){
  //   std::cout<<"ICHandler():: Unable to connect to FELIX endpoint. The felixcore application seems to be down."<<std::endl;
  //   throw std::runtime_error("ICHandler():: Unable to connect to FELIX endpoint. The felixcore application seems to be down.");
  //   abort();
  // }

  // m_rx=new netio::low_latency_subscribe_socket(m_context,[&](netio::endpoint& ep, netio::message& msg){
  //     //std::cout << "Received data from " << ep.address() << ":" << ep.port() << " size:" << msg.size() << endl;
  //     m_reply.clear();
  //     m_reply=msg.data_copy();
  //     //std::cout<<"Data is here"<<m_reply.size()<<std::endl;
  //   });
  // m_rx->subscribe(m_fID_R, netio::endpoint(m_flxHost, m_portFromGBTx));
}

ICNetioNextHandler::~ICNetioNextHandler()
{

  for (unsigned i=0;i<m_vec_fids.size();i++)
    m_NextClient->unsubscribe(m_vec_fids.at(i));

  delete m_NextClient;

}


void ICNetioNextHandler::setMaxRetries(int val) {
  if(val >= 0)
    m_maxRetries = val;
  else
    m_maxRetries = 2;
}

void ICNetioNextHandler::sendCfg(string fileName) {
  vector<uint8_t> data;
  ifstream fr(fileName);
  if (!fr.good()){
    cout << "File not found: " << fileName << endl;
    return;
  }
  data.reserve(1024);
  string line;
  while(getline(fr,line)){
    if(line.size()>2 || line.size()==0){
      cout << "Incorrect format" << endl;
      break;
    }
    data.push_back(strTouint8(line));
    if(data.size()>1023){
      cout << "File has too many lines: " << fileName << endl;
    }
  }
  fr.close();
  sendCfg( data );
}

void ICNetioNextHandler::sendCfg(const std::vector<uint8_t> &data) {
  std::vector<unsigned char> netioFrame = prepareICNetioFrame(false, 0, data);
  communicate(netioFrame);
}

void ICNetioNextHandler::sendRegs(std::map<std::string, std::vector<uint8_t>> regs) {
  

  //std::cout<<"Sending Registers"<<regs.size()<<std::endl;
  for(auto& reg : regs) {

    // char *cstr = new char[reg.first.length()+1];
    // strcpy(cstr,reg.first.c_str());
    const lpgbt_item_t * item = ParseRegister(reg.first.data(), m_icDevice);
    //delete cstr; 


    uint16_t RegVal = item->addr;
    std::vector<std::string> tmpVector;
    tmpVector.push_back(std::to_string(RegVal));

    std::vector<std::pair<std::string, uint8_t>> OrigVals;
    for(unsigned attempt=0; attempt < m_maxRetries; attempt++){
      OrigVals = readRegs(tmpVector);
      if (OrigVals.size()==0){
	cout<<"ICHandler - send Registers - Cannot read the original register value, trying again"<<std::endl;
	std::this_thread::sleep_for(std::chrono::milliseconds(50));
	if( (attempt+1) == m_maxRetries ) { 
	  abort(); //This points to a communcation issue
	  cout<<"ICHandler - send Registers - Reached maximum number of tries, aborting"<<endl;
	}
	continue;
      } else{
	break;
      }
      
    }
    uint8_t OrigVal = OrigVals.at(0).second;
    uint8_t Mask = (0xFF >> (8-item->nbits)) << item->bitindex;
    Mask = ~Mask;
    
    std::vector<uint8_t> sequence;
    for (auto &sreg : reg.second){


      uint8_t NewVal = OrigVal & Mask;
      uint8_t ChangedSec = (sreg << item->bitindex ) & (~Mask);
      NewVal = ChangedSec | NewVal;
      sequence.push_back(NewVal);      

    }    
    std::vector<unsigned char> netioFrame = prepareICNetioFrame(false, RegVal, sequence);
    communicate(netioFrame);
  }
}

std::vector<uint8_t> ICNetioNextHandler::readCfg() {
  std::cout<<"Starting read config"<<std::endl;
  std::vector<uint8_t> retVal;
  std::vector<uint8_t> dummy(464);
  
  std::vector<unsigned char> netioFrame = prepareICNetioFrame(true, 0, dummy);
  communicate(netioFrame);

  std::size_t FIRST_IC_PAYLOAD_BYTE = m_icDevice == DeviceType::lpGBT_v1 ? FIRST_IC_PAYLOAD_BYTE_V1 : FIRST_IC_PAYLOAD_BYTE_V0;
  //std::cout<<"reply: "<<m_reply.size()<<std::endl;
  // Remove FELIX and IC header words and parity trailer word
  if (m_reply.size()<=NUM_PARITY_BYTES_IC_TRAILER){
    std::cout<<"ICNetioNextHandler::readCfg() :: No response gotten from the command"<<std::endl; 
  }
  else{
    for (std::size_t i{FIRST_IC_PAYLOAD_BYTE};  i < m_reply.size() - NUM_PARITY_BYTES_IC_TRAILER; ++i) {
      retVal.push_back(m_reply.at(i));
    }
  }
  return retVal;
}

std::vector<std::pair<std::string, uint8_t>> ICNetioNextHandler::readRegs(std::vector<std::string> regs) {
  std::vector<std::pair<std::string, uint8_t>> retVal;

  const std::vector<uint8_t> dummy(1);
  std::size_t FIRST_IC_PAYLOAD_BYTE = m_icDevice == DeviceType::lpGBT_v1 ? FIRST_IC_PAYLOAD_BYTE_V1 : FIRST_IC_PAYLOAD_BYTE_V0;

  for (const auto& reg : regs) {
    
    // char *cstr = new char[reg.length()+1];
    // strcpy(cstr,reg.c_str());
    const lpgbt_item_t * item = ParseRegister(reg.data(), m_icDevice);
    // delete cstr; 
    uint16_t RegVal = item->addr;


    std::vector<unsigned char> netioFrame = prepareICNetioFrame(true, RegVal, dummy);

    communicate(netioFrame);
   
    // TODO: is this finished? it seems we are not retrieving the replies with the register values
    // TODO: test this!
    // Remove Felix header and GBTx header
    if (m_reply.size() > FIRST_IC_PAYLOAD_BYTE) {
      uint8_t tmpReply = m_reply.at(FIRST_IC_PAYLOAD_BYTE);
      uint8_t Mask = (0xFF >> (8-item->nbits)) << item->bitindex;
      tmpReply = Mask & tmpReply;
      tmpReply = tmpReply >> item->bitindex;
      retVal.emplace_back(reg, tmpReply);
    }
  }

  return retVal;
}


// TODO: what to do with i2c config?
std::vector<uint8_t> ICNetioNextHandler::prepareICNetioFrame(const bool read, const uint16_t startAddr, const std::vector<uint8_t>& data) {
  std::vector<uint8_t> retVal = {};
  
  size_t header_size = 6;
  if( m_icDevice==ICNetioNextHandler::DeviceType::lpGBT_v0 )
    header_size = 7;
  constexpr size_t footer_size = 1;


  if( m_icDevice==ICNetioNextHandler::DeviceType::lpGBT_v1 && data.size()>511 ){
    std::cout<<"Error data-size too big thats being tried to send to Netio"<<std::endl;
    return retVal;
  }
    
  if(!read)
    retVal.reserve(header_size + data.size() + footer_size);
  else
    retVal.reserve(header_size + footer_size);

  if( m_icDevice==ICNetioNextHandler::DeviceType::lpGBT_v0 )
    retVal.push_back(0); // Reserved in LpGBT v0
  retVal.push_back((m_i2cAddr << 1) + (read?0x1:0x0)); // GBTX I2C address and read/write bit
  retVal.push_back(1); // Command (not used in GBTX v1 + 2)
  retVal.push_back(data.size() & 0xFF); // Number of data bytes
  retVal.push_back((data.size() >> 8) & 0xFF);
    
  retVal.push_back(startAddr   & 0xFF); // Register (start) address
  retVal.push_back(startAddr >> 8 & 0xFF );

  if(!read)
    for(auto& val: data)
      retVal.push_back(val);

  // For GBTx, skip first 2 bytes in parity check (see above)
  std::size_t NUM_PARTITY_BYTES_SKIP{0};
  if( m_icDevice==ICNetioNextHandler::DeviceType::lpGBT_v0 )
    NUM_PARTITY_BYTES_SKIP=2;

  uint8_t parity = 0;
  for(size_t i = NUM_PARTITY_BYTES_SKIP;i < retVal.size(); ++i)
    parity ^= retVal[i];
  retVal.push_back(parity);


  return retVal;
}


bool ICNetioNextHandler::communicate(vector<uint8_t> & payload, uint32_t waitmillis){
  
  uint32_t attempt=0;
  while(true){
    
    m_reply.clear();
    m_NextClient->send_data(m_fID_CMD, payload.data(), payload.size(), true ); 
    std::this_thread::sleep_for(std::chrono::milliseconds(waitmillis));
    if(m_reply.size()) break;
    attempt++;
    if(attempt>(uint32_t)m_maxRetries){return false;}

    if(m_resubscribe){
      for(auto & fid: m_vec_fids)
	m_NextClient->unsubscribe(fid);
      m_NextClient->subscribe(m_vec_fids);
    }
  }
  return true;
}












uint32_t ICNetioNextHandler::CommTests(unsigned NTries, std::string Register="DACCAL2"){


  //Clear reply buffer
  m_reply.clear();
  m_clear=false; //Make sure clear is not called anymore
  std::size_t FIRST_IC_PAYLOAD_BYTE = m_icDevice == DeviceType::lpGBT_v1 ? FIRST_IC_PAYLOAD_BYTE_V1 : FIRST_IC_PAYLOAD_BYTE_V0;

  vector<uint8_t>  payload;
  char *cstr = new char[Register.length()+1];
  strcpy(cstr,Register.c_str());



  const lpgbt_item_t * item = ParseRegister(cstr, m_icDevice);
  uint16_t RegVal = item->addr;
  std::vector<std::string> tmpVector;
  tmpVector.push_back(std::to_string(RegVal));
  std::vector<std::pair<std::string, uint8_t>> OrigVals = readRegs(tmpVector);  
  uint8_t OrigVal = OrigVals.at(0).second;
  uint8_t Mask = (0xFF >> (8-item->nbits)) << item->bitindex;
  Mask = ~Mask;
  

  m_reply.clear();
  //Prepare the command to send readwrite over NTries
  for(unsigned ind=0;ind<NTries;ind++){

    uint8_t sreg = 2+ind%4; //ind%8;
    uint8_t NewVal = OrigVal & Mask;
    uint8_t ChangedSec = (sreg << item->bitindex ) & (~Mask);
    NewVal = ChangedSec | NewVal;
    std::vector<uint8_t> sequence;
    sequence.push_back(NewVal);      
    std::vector<unsigned char> netioFrame = prepareICNetioFrame(false, RegVal, sequence);



    payload.insert(payload.end(),netioFrame.begin(),netioFrame.end());
    payload.push_back(0xFF);
    // m_NextClient->send_data(m_fID_CMD, payload.data(), payload.size(), true ); 
    // payload.clear();

    netioFrame.clear();
    netioFrame = prepareICNetioFrame(true, RegVal, sequence);
    payload.insert(payload.end(),netioFrame.begin(),netioFrame.end());
    payload.push_back(0xFF);
    // m_NextClient->send_data(m_fID_CMD, payload.data(), payload.size(), true ); 
    // payload.clear();

    std::cout<<"Payload Size after read/write "<<payload.size()<<std::endl;
    std::cout<<"Replies: "<<m_reply.size()<<std::endl;
  }

  m_NextClient->send_data(m_fID_CMD, payload.data(), payload.size(), true ); 
  payload.clear();
  

  //Checking the readback responses that were recieved 
  
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  
  std::cout<<"Sent "<<NTries<<" read/write commands and recieved "<<m_reply.size()<<" replies."<<std::endl;


  if (m_reply.size() > FIRST_IC_PAYLOAD_BYTE) {
    uint8_t tmpReply = m_reply.at(FIRST_IC_PAYLOAD_BYTE);
    tmpReply = Mask & tmpReply;
    tmpReply = tmpReply >> item->bitindex;
    std::cout<<"Reg: "<<Register<<" Val: "<<int(tmpReply)<<std::endl;
  }

  m_clear=true; //Return back to old clear mode
  
  return NTries;





}
